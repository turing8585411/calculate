import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {


        while(true) {
            System.out.println("Enter the two numbers:");

            Scanner sc = new Scanner(System.in);
            float a = sc.nextFloat();
            float b = sc.nextFloat();


            System.out.println("Select a operation");
            System.out.println("1.Addition(+)");
            System.out.println("2.Subtract(-)");
            System.out.println("3.Multiply(*)");
            System.out.println("4.Division(/)");

            int n = sc.nextInt();
            if (n > 0 && n < 5) {
                if (n == 1) {
                    System.out.printf("%.2f", a + b);
                    System.out.println();
                }
                if (n == 2) {
                    System.out.printf("%.2f", a - b);
                    System.out.println();
                }
                if (n == 3) {
                    System.out.printf("%.2f", a * b);
                    System.out.println();
                }
                if (n == 4) {
                    System.out.printf("%.2f", a / b);
                    System.out.println();
                }
            } else {
                System.out.println("Please, enter the correct number!");

            }
        }

    }
}